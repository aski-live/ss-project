/*
 * Application
 */

jQuery(document).ready(function($){

    $('.header__left-menu').on('click', function(e) {

        e.preventDefault();

        if ( $(this).hasClass("open-menu") ) {

        	$(this).removeClass("open-menu");
        	$(".menu").removeClass("open-menu");

        } else {

        	$(this).addClass("open-menu");
        	$(".menu").addClass("open-menu");

        };

    });

});